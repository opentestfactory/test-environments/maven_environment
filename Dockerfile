FROM docker.io/maven:3.9.5-eclipse-temurin-21

COPY /requirements /tmp/requirements

ARG FIREFOX_VER=115.4.0esr
#Compatible with firefox>102 esr
ARG GECKODRIVER_VER=0.33.0
ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN=truex
ARG TZ=Europe/Paris
ARG APT_USER
ARG APT_PWD
ARG APT_CHROMIUM_REPOSITORY
ARG APT_REPOSITORY

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# 1. Prepare apt installation for chromium browser
# hadolint ignore=DL3008
RUN echo "deb [trusted=yes] https://$APT_USER:$APT_PWD@$APT_REPOSITORY" >> /etc/apt/sources.list.d/nexus.list \
    && echo "deb [trusted=yes] https://$APT_USER:$APT_PWD@$APT_CHROMIUM_REPOSITORY jammy main" >> /etc/apt/sources.list.d/nexus.list \
    && apt-get update && apt-get install -qqy software-properties-common \
# 2. Installation of X server, chromium, chromium-driver and dependancies
    && apt-get install --no-install-recommends -qqy  $(grep -vE "^\s*#" /tmp/requirements/apt-get-requirements-file.txt  | tr "\n" " ") \
    && rm -rf /var/lib/apt/lists/* \
    && ln -s /usr/bin/chromium-browser /usr/bin/chromium \
# 3. Create User otf with sudo rights and ssh connections
    && sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config \
    && sed -i 's/#PermitUserEnvironment no/PermitUserEnvironment yes/g' /etc/ssh/sshd_config \
    && useradd -m otf \
    && echo "otf ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers \
    && mkdir /home/otf/.ssh && touch /home/otf/.ssh/authorized_keys \
    && chown -R otf:otf /home/otf/ \
    && usermod --shell /bin/bash otf \
# 4. installation of Firefox
    && curl -fL -o /tmp/firefox.tar.bz2 \
        https://ftp.mozilla.org/pub/firefox/releases/${FIREFOX_VER}/linux-x86_64/fr/firefox-${FIREFOX_VER}.tar.bz2 \
    && tar -xjf /tmp/firefox.tar.bz2 -C /tmp/ \
    && chmod +x /tmp/firefox \
    && mv /tmp/firefox /usr/local/bin/ \
# 5. installation of geckodriver (Firefox driver)
    && curl -fL -o /tmp/geckodriver.tar.gz \
        https://github.com/mozilla/geckodriver/releases/download/v${GECKODRIVER_VER}/geckodriver-v${GECKODRIVER_VER}-linux64.tar.gz \
    && tar -xzf /tmp/geckodriver.tar.gz -C /tmp/ \
    && chmod +x /tmp/geckodriver \
    && mv /tmp/geckodriver /usr/local/bin/ \
# 6. Cleanup
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
                $toolDeps \
    && rm -rf /var/lib/apt/lists/* /tmp/* \
    && rm -f /etc/apt/sources.list.d/nexus.list

# 7. Prepare the ssh profile
COPY /skel/ /home/otf
COPY --chmod=755 /boot-scripts/ /home/otf
RUN mv /home/otf/environment /home/otf/.ssh

# 8. Ready to boot
USER otf
ENV PATH=$PATH:/usr/local/bin/firefox

WORKDIR /home/otf
EXPOSE 22
ENTRYPOINT [ "bash", "-c", "/home/otf/init.sh && sudo /home/otf/display.sh && sudo /usr/sbin/service ssh restart && sleep infinity"]

